let target = ['xmlUrl'],
    targetCount = target.length;

chrome.runtime.onMessage.addListener(function(message, callback) {
    if (message === 'setBadge') {
        setBadge();
    }
});

function setBadge()
{
    let data = [];

    chrome.storage.sync.get(target, function(googleData) {
        for (let i = 0; i < targetCount; i++) {
            data[target[i]] = googleData[target[i]] || null;
        }

        if (data['xmlUrl']) {
            let xhr = new XMLHttpRequest();
            let itemCount = 0;
            let isValidXml = false;

            xhr.open('GET', data['xmlUrl']);
            xhr.onload = function() {
                if (xhr.status === 200) {
                    if (xhr.responseXML) {
                        if (xhr.responseXML.getElementsByTagName("rss").length === 1) {
                            if (xhr.responseXML.getElementsByTagName("rss")[0].getElementsByTagName("channel").length === 1) {
                                itemCount  = xhr.responseXML.getElementsByTagName("rss")[0].getElementsByTagName("channel")[0].getElementsByTagName("item").length;
                                isValidXml = true;
                            }
                        }                     
                    }
                    
                    if (isValidXml === true) {
                        if (itemCount > 0) {
                            chrome.browserAction.setBadgeBackgroundColor({color:"#F62C0A"});
                            chrome.browserAction.setBadgeText({text: "청소!"});
                        } else {
                            chrome.browserAction.setBadgeText({text: ""});
                        }
                    } else {
                        chrome.browserAction.setBadgeBackgroundColor({color:"#8e8e8e"});  
                        chrome.browserAction.setBadgeText({text: "ERR"});
                    }
                } else {
                    chrome.browserAction.setBadgeBackgroundColor({color:"#B5B6B7"});  
                    chrome.browserAction.setBadgeText({text: "OFF"});
                }
            };
            xhr.onerror = function() {
                chrome.browserAction.setBadgeBackgroundColor({color:"#B5B6B7"});  
                chrome.browserAction.setBadgeText({text: "OFF"});
            }

            xhr.send();
        } else {
            chrome.browserAction.setBadgeText({text: ""});
        }
    });
}

window.onload = setBadge;

setInterval(function() {
    setBadge();
}, 1000 * 60 * 30);