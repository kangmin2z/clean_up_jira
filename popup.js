let target = ['xmlUrl'],
    targetCount = target.length;

// event binding
document.addEventListener('DOMContentLoaded', function() {
    let btnSaveAndReload = document.querySelector('#btnSaveAndReload');
    btnSaveAndReload.addEventListener('click', function() {
        saveAndReload();
    })
});

function saveAndReload()
{
    let data = {};
    
    for (let i = 0; i < targetCount; i++) {
        data[target[i]] = document.querySelector('#' + target[i]).value;
    }

    chrome.storage.sync.set(data, function() {
        chrome.runtime.sendMessage('setBadge', function() {});
        alert('저장 완료');
        window.close();
    });
}

function getAndSetData()
{
    let data = [];

    chrome.storage.sync.get(target, function(googleData) {
        for (let i = 0; i < targetCount; i++) {
            data[target[i]] = googleData[target[i]] || null;

            if (data[target[i]]) {
                document.querySelector('#' + target[i]).value = data[target[i]];
            }
        }
    });
}

window.onload = getAndSetData;