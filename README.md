* 웹스토어 : https://chrome.google.com/webstore/detail/clean-up-jira/cjginfeigkbdclbihdindilocgeikemk

* 테스트된 JIRA 버젼 : v6.2.5, v7.2.2 (XML 포맷이 다른 JIRA 버젼에서는 동작하지 않을 수 있다.)

* 개념 및 기능
    1. JQL을 이용해서 검색 결과가 0건이어야 정상인 쿼리를 만든다. (1건 이상이면 처리할 건이 있다는 것)
    2. JIRA의 기능을 이용해서 해당 결과를 XML로 출력하는 URL을 얻는다.
    3. 해당 URL을 본 익스텐션에 저장한다.
    4. 다음의 기능을 제공한다.
        * 해당 URL에 접근이 되고 처리해야할 건이 있으면 빨간색 뱃지로 "청소!"라고 뜬다.
        * 해당 URL에 접근이 되고 처리해야할 건이 없으면 뱃지가 없거나 사라진다.
        * 해당 URL에 접근은 되지만, XML이 아니거나 알 수 없은 XML 포맷이면 진한 회색 뱃지로 "ERR"라고 뜬다.
        * 해당 URL에 접근이 안되는 상황에서는 흐린 회색 뱃지로 "OFF"라고 뜬다.
            * URL을 잘못 입력한 경우
            * JIRA가 로그아웃인 상태
            * JIRA가 회사에서만 사용 허가된 경우, 외부에서 접근 시
        * 위 로직은 브라우저를 새로이 켰을 때와 30분 단위 무한루프로 동작한다.
        * 해당 저장값은 Google 서버에 저장되므로 브라우저 단위가 아닌 크롬 로그인 계정 단위로 다중 PC에서 사용할 수 있다.
        <br>
* 한계점
    * "청소!" 뱃지가 떠서 JIRA를 정리해서 0건으로 만들어도 바로 뱃지를 없앨 수 없다.
    * 아이콘을 클릭해서 [Save + Reload] 버튼을 누르던가 30분 무한루프로 제거될 때까지 기다려야 한다.
    * 30분 단위 무한루프 텀을 작게 줄이면 거의 실시간처럼 동작하게 할 수는 있겠지만 일단 그냥 둔다.
    <br>

* XML URL을 얻는 방법
    1. JIRA에서 JQL로 검색한다. (생성해둔 필터도 된다.)
    2. JQL 입력폼 우측 상단에 [보기]를 클릭한다. (한글판 v6.2.5 기준, v7.2.2 영문은 [Export])
    3. [보기] 하단에 열린 서브메뉴 중에서 [XML]을 클릭한다.
    4. "프로토콜://주소/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?jqlQuery=..." 형식의 XML URL로 전환된다.
    5. 위 주소를 통째로 복사해서 사용하면 된다. (http 또는 https 프로토콜 포함)
    <br>

* JQL 예제
    * 완료지연 : assignee = currentUser() AND status not in (Closed, Done, Resolved, Hold) AND due < startOfDay()
    * 본인에게 할당된 VOC/CS건 : assignee = currentUser() AND status not in (Closed, Done, Resolved, Hold) AND project = "VOC"
    <br>

* 귀찮거나 애매해서 안한 부분
    * "청소!"가 떴을 때 뭔가를 누르거나 하면 특정 JIRA URL로 이동이 필요하지 않을까 싶었지만, 그러려면 입력받을 값이 더 필요해서 귀찮을 것이고 본 익스텍션을 사용한다면 대시보드던 필터던 만들어 둔 사용자일 거라고 생각하고 안 만들었다.